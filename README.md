# flying_lego_ws

```
roslaunch flying_lego_predictor lego_predict.launch 
```

run rosbag

```
rosbag play 2023-04-21-12-27-11.bag --clock
```

can also slow the simulation time with '-r' ex. 20%
```
rosbag play 2023-04-21-12-27-11.bag --clock -r 0.2
```