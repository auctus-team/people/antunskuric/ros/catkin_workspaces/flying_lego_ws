#!/usr/bin/env python
import rospy
import tf2_ros
import geometry_msgs.msg
import numpy as np


def publish_tf(pos, name, br, orientation):
    t = geometry_msgs.msg.TransformStamped()

    t.header.stamp = rospy.Time.now()
    t.header.frame_id = "world"
    t.child_frame_id = name
    t.transform.translation.x = pos[0]
    t.transform.translation.y = pos[1]
    t.transform.translation.z = pos[2]

    t.transform.rotation.x = orientation.x
    t.transform.rotation.y = orientation.y
    t.transform.rotation.z = orientation.z
    t.transform.rotation.w = orientation.w

    br.sendTransform(t)


def predictor():
    rospy.init_node('predictor', anonymous=True)


    tfBuffer = tf2_ros.Buffer()
    listener = tf2_ros.TransformListener(tfBuffer)


    br = tf2_ros.TransformBroadcaster()

    rate = rospy.Rate(100) # 10hz

    dt = 0.01
    p_old = np.array([0, 0, 0])
    v_old = np.array([0, 0, 0])
    g = np.array([0,0,-9.81])
    t_last = 0
    while not rospy.is_shutdown():

        try:
            trans = tfBuffer.lookup_transform('world', 'lego1', rospy.Time(0))
            trans.header.stamp 
        except (tf2_ros.LookupException, tf2_ros.ConnectivityException, tf2_ros.ExtrapolationException):
            # print('no lego found')
            continue

        t_c = trans.header.stamp.to_sec()
        if t_c == t_last:
            continue
        dt = t_c - t_last
        t_last = t_c
        p = np.array([trans.transform.translation.x, trans.transform.translation.y, trans.transform.translation.z])
        v = (p - p_old)/dt
        a = (v - v_old)/dt
        v_old = v
        p_old = p
        if np.linalg.norm(a-g) < 10: 

            dt1 = 0.1
            dt2 = 0.3
            dt3 = 0.5
            p1 = p + v * dt1 + np.array([0,0,-9.81]) * dt1 * dt1 / 2
            p2 = p + v * dt2 + np.array([0,0,-9.81]) * dt2 * dt2 / 2
            p3 = p + v * dt3 + np.array([0,0,-9.81]) * dt3 * dt3 / 2

            for i, dt1 in enumerate(np.linspace(0,1,10)):
                p1 = p + v * dt1 + np.array([0,0,-9.81]) * dt1 * dt1 / 2
                publish_tf(p1 , 'lego'+str(i)+'_predicted', br, trans.transform.rotation)
        rate.sleep()

if __name__ == '__main__':
    try:
        predictor()
    except rospy.ROSInterruptException:
        pass