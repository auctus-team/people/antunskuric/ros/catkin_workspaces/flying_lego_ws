#!/usr/bin/env python
import rospy
import tf2_ros
from scipy.spatial.transform import Rotation as R
import geometry_msgs.msg
import visualization_msgs.msg
import numpy as np

n = np.array([1,0,1])
n = n/np.linalg.norm(n)
d = 0.3


def publish_marker(n, d):
    m = visualization_msgs.msg.Marker()
    m.header.stamp = rospy.Time.now()
    m.header.frame_id = "world"
    m.id = 0
    m.type = m.CUBE
    pos = n*d

    u,s,v = np.linalg.svd(n.reshape(1,-1))
    m.pose.position.x = pos[0]
    m.pose.position.y = pos[1]
    m.pose.position.z = pos[2]

    ro = R.from_matrix(-v)
    orientation = ro.as_quat()
    m.pose.orientation.x = orientation[0]
    m.pose.orientation.y = orientation[1]
    m.pose.orientation.z = orientation[2]
    m.pose.orientation.w = orientation[3]

    m.scale.x = 0.01
    m.scale.y = 2
    m.scale.z = 2

    m.color.a = 0.2
    m.color.r = 1.0
    m.color.g = 0.0
    m.color.b = 0.0

    # publish marker
    p = rospy.Publisher('marker', visualization_msgs.msg.Marker, queue_size=10)
    p.publish(m)

def publish_tf(pos, name, br, orientation):
    t = geometry_msgs.msg.TransformStamped()

    t.header.stamp = rospy.Time.now()
    t.header.frame_id = "world"
    t.child_frame_id = name
    t.transform.translation.x = pos[0]
    t.transform.translation.y = pos[1]
    t.transform.translation.z = pos[2]

    t.transform.rotation.x = orientation.x
    t.transform.rotation.y = orientation.y
    t.transform.rotation.z = orientation.z
    t.transform.rotation.w = orientation.w

    br.sendTransform(t)


def predictor():
    rospy.init_node('intersector', anonymous=True)

    tfBuffer = tf2_ros.Buffer()
    listener = tf2_ros.TransformListener(tfBuffer)


    br = tf2_ros.TransformBroadcaster()



    rate = rospy.Rate(50) # 10hz

    dt = 0.01
    p_old = np.array([0, 0, 0])
    v_old = np.array([0, 0, 0])
    g = np.array([0,0,-9.81])
    t_last = 0
    a = np.zeros(3)
    while not rospy.is_shutdown():
        publish_marker(n,d)

        try:
            trans = tfBuffer.lookup_transform('world', 'lego1', rospy.Time(0))
            trans.header.stamp 
        except (tf2_ros.LookupException, tf2_ros.ConnectivityException, tf2_ros.ExtrapolationException):
            # print('no lego found')
            continue

        t_c = trans.header.stamp.to_sec()
        if t_c == t_last:
            continue
        dt = t_c - t_last
        t_last = t_c
        p = np.array([trans.transform.translation.x, trans.transform.translation.y, trans.transform.translation.z])
        v = (p - p_old)/dt 
        a = (v - v_old)/dt 
        v_old = v
        p_old = p

        if np.linalg.norm(a-g) < 10: 

            np0 = n@p 
            nv0 = n@v
            na0 = n@g

            try:
                t = (-nv0 + np.sqrt(nv0*nv0 - 2*na0*(np0-d)))/na0
                t2 = (-nv0 - np.sqrt(nv0*nv0 - 2*na0*(np0-d)))/na0
            except:
                continue
        
            if t < 0:
                t = t2

            if not np.isfinite(t):
                continue

            p = p + v * t + g * t * t / 2

            publish_tf(p , 'intersection', br, trans.transform.rotation)
        rate.sleep()

if __name__ == '__main__':
    try:
        predictor()
    except rospy.ROSInterruptException:
        pass